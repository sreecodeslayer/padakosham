from django import forms
from crispy_forms.helper import FormHelper

from .models import Word, Meaning


class WordAddForm(forms.ModelForm):

    
    class Meta:
    	model = Word
    	fields = [
        'text', 'etymology', 'homonyms', 'origin_language',
        'place', 'plural', 'singular', 'root',]

    def __init__(self, *args, **kwargs):
        super(WordAddForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.fields['homonyms'].widget = forms.TextInput(
            attrs={'placeholder':'ഈ വാക്കിന്റെ വിവിധ അർത്ഥങ്ങൾ കോമ ഉപയോഗിച്ച് വേർതിരിച്ച്  ചേർക്കുക'}
        )
        self.fields['place'].widget = forms.TextInput(
            attrs={'placeholder':'ഈ വാക്ക് ഒരു പ്രാദേശികഭേദമാണെങ്കിൽ മാത്രം ഏതു പ്രദേശത്താണ്  പ്രചാരത്തിലുള്ളത് എന്ന് ചേർക്കുക'}
        )
        self.fields['plural'].widget = forms.TextInput(
            attrs={'placeholder':'ഈ വാക്കിന്റെ ബഹുവചനം എന്താണ്?'}
        )
        self.fields['singular'].widget = forms.TextInput(
            attrs={'placeholder':'ഈ വാക്കിന്റെ ഏകവചനം എന്താണ്?'}
        )
        self.fields['root'].widget = forms.TextInput(
            attrs={'placeholder':'ഈ വാക്കിന്റെ മൂലപദം എന്തെന്ന് കണ്ടെത്തി ചേർക്കുക'}
        )

class WordMeaningAddForm(forms.ModelForm):

    class Meta:
        model = Meaning
        fields = [
            'text', 'gender', 'category',
            'examples', 'synonyms', 'opposites',

        ]

    def __init__(self, *args, **kwargs):
        super(WordMeaningAddForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.fields['synonyms'].widget = forms.TextInput(
            attrs={'placeholder':'ഈ വാക്കിന്റെ വിവിധ പര്യായങ്ങൾ കോമ ഉപയോഗിച്ച് വേർതിരിച്ച്  ചേർക്കുക'}
        )
        self.fields['opposites'].widget = forms.TextInput(
            attrs={'placeholder':'ഈ ചേർക്കുന്ന അർത്ഥത്തിന്റെ വിപരീതാർത്ഥം വരുന്ന വാക്ക് ചേ‌‌ർക്കുക'}
        )

class StudentAddForm(forms.Form):
    name = forms.CharField(label='Your name', max_length=100)
    grade = forms.CharField(label='Your name', max_length=100)
    school =  forms.CharField(label='Your name', max_length=100)